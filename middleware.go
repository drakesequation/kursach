package main

import (
	"bytes"
	"crypto/sha1"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/gocraft/web"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"
)

var (
	cookieName = "SC_RAYS_COOKIE"
)

func (c *Context) BetaAccessCheckMiddleware(rw web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
	c.authorized = false
	if strings.Contains(c.Cookie, "SC_") {
		//	c.authorized =  true;
		c.Auth = "by-cookie"
	}
	next(rw, r)

}

func (c *Context) BetaAccessMiddleware(rw web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
	if len(c.Cookie) < 1 {
		expiration := time.Now().Add(15 * time.Minute)
		ip, _, err := net.SplitHostPort(r.RemoteAddr)
		if err != nil {
			log.Errorf("bad ip - rays error")
		}
		shortId, longId, _ := getRayId(r.Header.Get("User-Agent"), ip)
		cookie := http.Cookie{Name: cookieName, Value: longId, Expires: expiration}
		http.SetCookie(rw, &cookie)
		c.authorized = false
		c.Auth = fmt.Sprintf("Checking for Auth on %s", shortId)
	}

	log.Debugf("tmp: auth status: %v", c.authorized)
	if c.authorized {
		next(rw, r)
		return
	}

	var res bytes.Buffer

	tt := t["rays"]
	tt.Execute(&res, c)
	fmt.Fprintf(rw, res.String())
}

func (c *Context) SkipMiddleware(rw web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
	c.Skip = 0
	c.id = 0

	if s, err := strconv.ParseInt(r.PathParams["skip"], 10, 32); err == nil {
		c.Skip = s
	}
	if s, err := strconv.ParseInt(r.PathParams["id"], 10, 32); err == nil {
		c.id = s
	}

	next(rw, r)

}

func getRayId(ua string, ip string) (shortId string, longId string, err error) {
	err = nil
	shortId = ""
	longId = ""

	ut := int32(time.Now().Unix())

	shortPeriod := ut / 1890 // 31.5
	longPeriod := ut / 41400 // 11.5

	shortId = fmt.Sprintf("salt-%d,%s,%s", shortPeriod, ua, ip)
	longId = fmt.Sprintf("salt-%d,%s,%s", longPeriod, ua, ip)

	h := sha1.New()

	h.Write([]byte(shortId))

	log.Infof("Display this hash: %s, set this hash to cookie/check against it: %s", shortId, longId)

	shortId = fmt.Sprintf("SC-%x", h.Sum(nil))
	h = sha1.New()
	h.Write([]byte(longId))

	longId = fmt.Sprintf("SC-%x", h.Sum(nil))

	log.Infof("[cryptic] Display this hash: %s, set this hash to cookie/check against it: %s", shortId, longId)

	return
}
