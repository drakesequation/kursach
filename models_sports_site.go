package main

import (
	//	"github.com/jinzhu/gorm"
	"time"
)

type User struct {
	CleanJSONModel
	Login string
	About string
	// поля про ассоциацию с тем или иным
	Athlete       *Athlete
	AtheteID      int
	Coach         *Coach
	CoachID       int
	Team          *Team
	TeamID        int
	Club          *Club
	Competition   *Competition
	CompetitionID int
	// права доступа
	IsSiteAdmin bool
	IsOrganizer bool
	// пароль
	Password    string
}

type Newspost struct {
	CleanJSONModel
	Created time.Time
	Title   string
	Content string

	LocationID    int
	Location      *Location
	AthleteID     int
	Athlete       *Athlete
	CoachID       int
	Coach         *Coach
	TeamID        int
	Team          *Team
	ClubID        int
	Club          *Club
	CompetitionID int
	Competition   *Competition
}

type GeoPoint struct {
	CleanJSONModel
	Name      string
	Lat       string
	Lng       string
	CanStart  bool
	CanFinish bool
}

type GeoRoute struct {
	CleanJSONModel
	Name        string
	LengthKM    string
	Description string
	POI         []GeoPoint `gorm:"many2many:pois_routes;"`
}
