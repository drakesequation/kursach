package main

import (
	"bytes"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/gocraft/web"
	"net/http"
)

type Paginated struct {
	Count  int64
	Status int
	Error  string `json:",omitempty"`
}

func (c *Context) NotFound(rw web.ResponseWriter, r *web.Request) {
	if !c.authorized {
		log.Warnf("404 and not authorized")
		rw.WriteHeader(http.StatusNotFound)
		return
	}
	rw.WriteHeader(http.StatusNotFound)

	var res bytes.Buffer

	tt := t["service/404"]
	tt.Execute(&res, c)
	fmt.Fprintf(rw, res.String())
}

func (c *Context) RenderLangPage(rw web.ResponseWriter, r *web.Request) {
	c.PageID = r.PathParams["page_id"]
	pgina := c.PageID
	switch pgina {
	case "index":
		pgina = "index/index"
		break
	case "projects":
		pgina = "projects/projects"
		break
	case "about_us":
		pgina = "us/about"
		break
	case "contact_us":
		pgina = "us/contact"
		break
	}
	var res bytes.Buffer

	template := getTemplate(pgina)
	if template == nil {
		log.Errorf("non existant page requested: %v", r.URL)
		http.Redirect(rw, r.Request, "/404", http.StatusSeeOther)
		return
	}
	e := template.Execute(&res, c)
	log.Infof("%v", e)
	fmt.Fprintf(rw, res.String())

}

func (c *Context) ChetContextMiddleware(rw web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
	c.PageTitle = "Rnd"
	c.PageID = "index"
	cc, err := r.Cookie(cookieName)
	if err != nil {
		c.Cookie = ""
	} else {
		c.Cookie = cc.String()
	}
	next(rw, r)
}

func (c *Context) RenderIndex(rw web.ResponseWriter, r *web.Request) {
	http.Redirect(rw, r.Request, fmt.Sprintf("/pages/ru/index"), http.StatusSeeOther)

}
func (c *Context) RenderTestPage(rw web.ResponseWriter, r *web.Request) {
	var res bytes.Buffer

	tt := t["service/test"]
	tt.Execute(&res, c)
	fmt.Fprintf(rw, res.String())
}
