package main

import (
	//"github.com/jinzhu/gorm"
	"time"
)

type ChangeRank struct {
	CleanJSONModel
	Date_Of_Receiving time.Time
	Current           bool
	Athlete           *Athlete
	AthleteID         int
	Rank              *Rank
	RankID            int
}

type AthleteInTeam struct {
	CleanJSONModel
	Current     bool
	Date_Joined time.Time
	Athlete     *Athlete
	AthleteID   int
	Team        *Team
	TeamID      int
}

type AthleteInClub struct {
	CleanJSONModel
	Current     bool
	Date_Joined string
	Athlete     *Athlete
	AthleteID   int
	Club        *Club
	ClubID      int
}

type TeamInCompetition struct {
	CleanJSONModel
	Score         string
	Place         string
	Comment       string
	Team          *Team
	TeamID        int
	Competition   *Competition
	CompetitionID int
}
