package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	log "github.com/Sirupsen/logrus"
	"github.com/gocraft/web"
	"net/http"
)

type NewsViewJS struct {
	Paginated
	News []Newspost
}

func (c *Context) RenderNews(rw web.ResponseWriter, r *web.Request) {
	C := make([]Newspost, 0)
	var count int64
	MainDB.Model(&Newspost{}).Count(count)

	MainDB.Set("gorm:auto_preload", true).Offset(c.Skip).Limit(10).Find(&C)

	template := getTemplate("news/list")
	if template == nil {
		log.Errorf("non existant page requested: %v", r.URL)
		http.Redirect(rw, r.Request, "/404", http.StatusSeeOther)
		return
	}
	var res bytes.Buffer
	e := template.Execute(rw, &c)
	log.Infof("%v", e)
	fmt.Fprintf(rw, res.String())

}

func (c *Context) RenderNewsPostById(rw web.ResponseWriter, r *web.Request) {

	MainDB.Set("gorm:auto_preload", true).Where(&CleanJSONModel{ID: uint(c.id)}).Find(&c.Newspost)

	template := getTemplate("news/postpage")
	if template == nil {
		log.Errorf("non existant page requested: %v", r.URL)
		http.Redirect(rw, r.Request, "/404", http.StatusSeeOther)
		return
	}
	var res bytes.Buffer

	e := template.Execute(&res, c)
	log.Infof("%v", e)
	fmt.Fprintf(rw, res.String())

}

func (c *Context) RenderNewsJSON(rw web.ResponseWriter, r *web.Request) {
	C := NewsViewJS{}
	MainDB.Model(&Newspost{}).Count(&C.Count)
	C.Count = int64(C.Count) + int64(((2 + c.Skip) * 10))
	MainDB.Set("gorm:auto_preload", true).Offset(c.Skip).Limit(10).Find(&C.News)
	/*
	   for i := c.Skip*10; i < (c.Skip+1)*10; i++ {
	       N := Newspost{Title: fmt.Sprintf("Тестовая новость %d", i), Content: fmt.Sprintf("%d == %d", i, i)}
	       //N.ID = i
	       C.News = append(C.News, N)
	   }*/
	out, _ := json.MarshalIndent(&C, "", "  ")
	fmt.Fprintf(rw, string(out))
}
