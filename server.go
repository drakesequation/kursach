package main

import (
	log "github.com/Sirupsen/logrus"
	"github.com/eknkc/amber"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gopkg.in/fsnotify.v0"
	"html/template"
	"net/http"
	"sync"
)

const (
	baseDir = "templates/"
)

var (
	compiler = amber.New()
	t        map[string]*template.Template
	r        sync.RWMutex
	MainDB   *gorm.DB
)

func getTemplate(name string) *template.Template {
	r.RLock()
	defer r.RUnlock()
	if t, ok := t[name]; ok {
		return t
	}
	return nil
}

func readTemplates() {
	var err error
	t, err = amber.CompileDir(baseDir, amber.DefaultDirOptions, amber.DefaultOptions)
	if err != nil {
		log.Fatalf("%v", err)
	}
}

func main() {
	// База
	db, err := gorm.Open("sqlite3", "orients.db")
	if err != nil {
		panic("failed to connect database")
	}
	MainDB = db
	db.LogMode(true)

	// Синхронизация моделей
	db.AutoMigrate(
		&Athlete{},
		&Coach{},
		&Club{},
		&Location{},
		&Team{},
		&Rank{},
		&Competition{},
		&Competitiontype{},
		&ChangeRank{},
		&AthleteInClub{},
		&AthleteInTeam{},
		&TeamInCompetition{},
		&User{},
		&Newspost{},
		&GeoPoint{},
		&GeoRoute{},
	)

	// Маршруты
	router := createRouter()

	// Шаблоны
	readTemplates()

	// Мониторить изменения файлов шаблонов
	defer watcher.Close()
	watcher.Watch(baseDir)

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}

	// Установить админский интерфейс
	mux := http.NewServeMux()
	getAdmin(db).MountTo("/admin", mux)
	go http.ListenAndServe("127.0.0.1:13121", mux)

	// Сервер
	go http.ListenAndServe("0.0.0.0:13120", router)

	for {
		select {
		case event := <-watcher.Event: // перечитать шаблоны
			log.Infof("file at %s updated", event.Name)
			r.Lock()
			readTemplates()
			r.Unlock()

		case err := <-watcher.Error:
			log.Errorf("%v", err)
		}
	}
}
