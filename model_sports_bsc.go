package main

import (
	//	"github.com/jinzhu/gorm"
	"database/sql"
	"time"
)

type CleanJSONModel struct {
	ID        uint       `gorm:"primary_key"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `sql:"index" json:"-"`
}

type Location struct {
	CleanJSONModel
	City    string
	Country string
	POI     []GeoPoint `gorm:"many2many:locations_pois;"`
}

type Coach struct {
	CleanJSONModel
	Name       string
	Surname    string
	Location   *Location `json:"-"`
	LocationID int
}

type Athlete struct {
	CleanJSONModel
	Name          string
	Surname       string
	Date_Of_Birth string
	CoachID       int `json:"-"`
	Coach         *Coach
}

type Club struct {
	CleanJSONModel
	Name     string
	Location Location
}

type Team struct {
	CleanJSONModel
	Name     string
	Location Location
}

type Competitiontype struct {
	CleanJSONModel
	Name        string
	Description string
}

type Rank struct {
	CleanJSONModel
	Level string
}

type Competition struct {
	CleanJSONModel
	Date              string
	Name              string
	Location          *Location `json:"-"`
	LocationID        sql.NullInt64
	Competitiontype   *Competitiontype
	CompetitiontypeID sql.NullInt64 `json:"-"`
}
