package main

type Context struct {
	PageTitle  string
	PageID     string
	Cookie     string
	Auth       string
	authorized bool // защита бета доступа

	// сайтовое
	Loggedin bool // залогинен ли юзер
	User     User
	id       int64
	Skip     int64

	// спорт
	News     []Newspost
	Newspost Newspost
}
