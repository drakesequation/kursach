package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/qor/admin"
	"github.com/qor/qor"
)

func getAdmin(db *gorm.DB) *admin.Admin {
	// Initalize
	Admin := admin.New(&qor.Config{DB: db})
	Admin.SetSiteName("Ориентирование Админ")

	Admin.AddResource(&Athlete{}, &admin.Config{Name: "Спортсмены", Menu: []string{"Ориентирование"}})
	//	mdmuz :=
	Admin.AddResource(&Coach{}, &admin.Config{Name: "Тренеры", Menu: []string{"Ориентирование"}})

	Admin.AddResource(&Location{}, &admin.Config{Name: "Локации", Menu: []string{"Ориентирование"}})

	Admin.AddResource(&Club{}, &admin.Config{Name: "Клубы", Menu: []string{"Ориентирование"}})

	Admin.AddResource(&Team{}, &admin.Config{Name: "Команды", Menu: []string{"Ориентирование"}})

	Admin.AddResource(&Rank{}, &admin.Config{Name: "Ранги", Menu: []string{"Ориентирование"}})

	xy := Admin.AddResource(&Competition{}, &admin.Config{Name: "Соревнования", Menu: []string{"Ориентирование"}})
	xy.Meta(&admin.Meta{Name: "Name", Label: "Название мероприятия"})
	xy.Meta(&admin.Meta{Name: "Competitiontype", Label: "Тип соревнования", Config: &admin.SelectOneConfig{AllowBlank: true}})

	xx := Admin.AddResource(&Competitiontype{}, &admin.Config{Name: "Типы соревнований", Menu: []string{"Ориентирование"}})
	xx.Meta(&admin.Meta{Name: "Description", Label: "никто не увидит", Type: "rich_editor"})

	//
	Admin.AddResource(&ChangeRank{}, &admin.Config{Name: "ранги спортсмена", Menu: []string{"Прогресс и участие"}})

	Admin.AddResource(&AthleteInTeam{}, &admin.Config{Name: "команды спортсмена", Menu: []string{"Прогресс и участие"}})

	Admin.AddResource(&AthleteInClub{}, &admin.Config{Name: "клубы спортсмена", Menu: []string{"Прогресс и участие"}})

	Admin.AddResource(&TeamInCompetition{}, &admin.Config{Name: "Команды в соревнованиях, результаты", Menu: []string{"Прогресс и участие"}})

	Admin.AddResource(&Newspost{}, &admin.Config{Name: "Новости", Menu: []string{"Сайт"}})
	user := Admin.AddResource(&User{}, &admin.Config{Name: "Пользователи", Menu: []string{"Сайт"}})
	Admin.AddResource(&GeoPoint{}, &admin.Config{Name: "Геоточки для локаций", Menu: []string{"Сайт"}})

	/* &Athlete{},
	   &Coach{}, &Club{},
	   &Location{}, &Team{},
	   &Rank{}, &Competition{},
	   &CompetitionType{},	&ChangeRank{},&AthleteInClub{},&AthleteInTeam{},&TeamInCompetition{},	&User{},&Newspost{} */

	//	users := Admin.AddResource(&SiteUser{}, &admin.Config{Menu: []string{"Site Users"}})
	user.Meta(&admin.Meta{Name: "Password", Type: "password"})

	return Admin

}
