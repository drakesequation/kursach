package main

import (
	log "github.com/Sirupsen/logrus"
	"github.com/gocraft/web"
	"os"
	"path"
)

func createRouter() *web.Router {
	currentRoot, _ := os.Getwd()
	ass := path.Join(currentRoot, "./public")
	log.Infof("assets path: %s", ass)

	router := web.New(Context{})

	router.Middleware(web.LoggerMiddleware)
	router.Middleware((*Context).ChetContextMiddleware)
	router.Middleware(web.LoggerMiddleware)
	router.Middleware(web.StaticMiddleware(ass, web.StaticOption{IndexFile: "index.html"}))

	// router.Middleware((*Context).BetaAccessCheckMiddleware)
	// router.Middleware((*Context).BetaAccessMiddleware)

	router.Get("/", (*Context).RenderIndex)

	pages := router.Subrouter(Context{}, "/pages")
	pages.Middleware((*Context).SkipMiddleware)

	pages.Get("/news/post/:id", (*Context).RenderNewsPostById)
	pages.Get("/news", (*Context).RenderNews)
	pages.Get("/news/:skip", (*Context).RenderNews)

	api := router.Subrouter(Context{}, "/api")
	api.Middleware((*Context).SkipMiddleware)
	//	api.Middleware((*Context).APILoginMiddleware)
	api.Get("/competitions/", (*Context).RenderCompetitionsJSON)
	api.Get("/competitions/:skip", (*Context).RenderCompetitionsJSON)

	api.Get("/news/", (*Context).RenderNewsJSON)
	api.Get("/news/:skip", (*Context).RenderNewsJSON)

	router.NotFound((*Context).NotFound)

	return router
}
