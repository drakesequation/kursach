package xyz.runas.orient;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemSpanLookup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static java.net.Proxy.Type.HTTP;

public class MainActivity extends MenuedActivity {
    RecyclerIxAdapter adapter;

    class NewsJSONAsyncTask extends AsyncTask<String, Void, Boolean> {
        ArrayList<NewsPost> nn = new ArrayList<>();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        private final OkHttpClient client = new OkHttpClient();

        @Override
        protected Boolean doInBackground(String... urls) {
            try {
                Request request = new Request.Builder()
                        .url(urls[0])
                        .build();

                Response response = client.newCall(request).execute();
                m_isLoading = false;

                if (!response.isSuccessful()) {
                } else {


                    JSONObject jsono = new JSONObject(response.body().string());
                    Log.d("JSON", jsono.toString());


                    int total = jsono.getInt("Count");

                    JSONArray jarray = jsono.getJSONArray("News");


                    for (int i = 0; i < jarray.length(); i++) {
                        JSONObject newspost = jarray.getJSONObject(i);
                        NewsPost N = new NewsPost();
                        N.title = newspost.getString("Title");
                        N.text = newspost.getString("Content");
                        nn.add(N);
                    }

                    return true;
                }

          } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return false;
        }
        protected void onPostExecute(Boolean result) {
            Message msg = handler.obtainMessage(1);
            msg.obj = nn;
            msg.sendToTarget();

            if (result == false) {
            }
        }
    }


    private static class NewsPostVH extends RecyclerView.ViewHolder {
        TextView tvTitle, tvText, tvDate;
        String url;

        public NewsPostVH(View view, final RecyclerOnItemClickListener recyclerOnItemClickListener) {
            super(view);
            this.tvTitle = (TextView) view.findViewById(R.id.tv_title);
            this.tvText = (TextView) view.findViewById(R.id.tv_text);
            this.tvDate = (TextView) view.findViewById(R.id.tv_date);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (recyclerOnItemClickListener != null) {
                        recyclerOnItemClickListener.onItemClicked(v, getAdapterPosition());
                    }
                }
            });
        }
    }

    private static class NewsPost {
        public String title, text, date;
        public int id;
    }

    private class RecyclerIxAdapter extends RecyclerView.Adapter<NewsPostVH> implements RecyclerOnItemClickListener {
        private final List<NewsPost> data;

        public RecyclerIxAdapter(List<NewsPost> data) {
            this.data = data;
        }

        @Override
        public NewsPostVH onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_list_item, parent, false);
            return new NewsPostVH(view, this);

        }

        @Override
        public void onBindViewHolder(NewsPostVH holder, int position) {
            NewsPost post = data.get(position);
            holder.tvText.setText(post.text);
            holder.tvTitle.setText(post.title);
            holder.tvDate.setText(post.date);
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public void add(List<NewsPost> items) {
            int previousDataSize = this.data.size();
            this.data.addAll(items);
            notifyItemRangeInserted(previousDataSize, items.size());
        }

        public void add(NewsPost item) {
            int previousDataSize = this.data.size();

            this.data.add(item);
            notifyItemRangeInserted(previousDataSize, 1);
        }

        @Override
        public void onItemClicked(View view, int position) {
            Intent i = new Intent(MainActivity.this, Browser.class);
            i.putExtra("url", "http://orient.runas.xyz/pages/news/post/" + this.data.get(position).id);
            startActivity(i);
         //   this.data.remove(position);
         //   notifyItemRemoved(position);
        }

    }



    private Runnable fakeCallback = new Runnable() {
        @Override
        public void run() {

            new NewsJSONAsyncTask().execute(
                    "http://orient.runas.xyz/api/news/" + m_page);

            m_page++;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        m_layout = R.layout.activity_main;
        super.onCreate(savedInstanceState);

        rvNews = findViewById(R.id.rvNews);
        handler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    ArrayList<NewsPost> nn =  (ArrayList<NewsPost>)msg.obj;
                    adapter.add(nn);
                    adapter.notifyDataSetChanged();

                }
            }
        };
        Paginate.Callbacks callbacks = new Paginate.Callbacks() {
            @Override
            public void onLoadMore() {
                // Load next page of data (e.g. network or database)
                m_isLoading = true;
                handler.postDelayed(fakeCallback, 5000);
            }

            @Override
            public boolean isLoading() {
                // Indicate whether new page loading is in progress or not
                return m_isLoading;
            }

            @Override
            public boolean hasLoadedAllItems() {
                // Indicate whether all data (pages) are loaded or not
                return m_loadedAll;
            }
        };
        int layoutOrientation;
        layoutOrientation = OrientationHelper.VERTICAL;
        adapter = new RecyclerIxAdapter(new ArrayList<NewsPost>());


        RecyclerView.LayoutManager layoutManager = null;
        layoutManager = new LinearLayoutManager(this, layoutOrientation, false);

        rvNews.setLayoutManager(layoutManager);

        rvNews.setAdapter(adapter);


        Paginate.with(rvNews, callbacks)
                .setLoadingTriggerThreshold(2)
                .addLoadingListItem(true)
                .setLoadingListItemCreator(new CustomLoadingListItemCreator())
                .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                    @Override
                    public int getSpanSize() {
                        return 1;
                    }
                })
                .build();


    }

    private RecyclerView rvNews = null;
}
