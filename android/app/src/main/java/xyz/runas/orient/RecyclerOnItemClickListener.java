package xyz.runas.orient;

import android.view.View;

/**
 * Created by dev on 3/15/18.
 */


public interface RecyclerOnItemClickListener {
    void onItemClicked(View view, int position);
}