package xyz.runas.orient;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class Browser extends MenuedActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        m_layout = R.layout.activity_browser;
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_browser);
        WebView webview = (WebView)findViewById(R.id.wvBrowser);

        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
             //   activity.setProgress(progress * 1000);
            }
        });
        webview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
              //  Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }
        });
        String url= getIntent().getStringExtra("url");
        Toast.makeText(this.getApplicationContext(), url, Toast.LENGTH_LONG).show();
        webview.loadUrl(url);
    }
}
