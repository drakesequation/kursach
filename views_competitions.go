package main

import (
	"fmt"
	//log "github.com/Sirupsen/logrus"
	"encoding/json"
	"github.com/gocraft/web"
)

type CompsView struct {
	Paginated
	Competitions     []Competition
	CompetitionTypes map[int64]string
}

func (c *Context) RenderCompetitionsJSON(rw web.ResponseWriter, r *web.Request) {
	C := CompsView{CompetitionTypes: make(map[int64]string)}
	MainDB.Model(&Competition{}).Count(&C.Count)
	MainDB.Set("gorm:auto_preload", true).Offset(c.Skip).Limit(10).Find(&C.Competitions)

	for _, v := range C.Competitions {
		C.CompetitionTypes[v.CompetitiontypeID.Int64] = "???"
	}

	out, _ := json.MarshalIndent(&C, "", "  ")
	fmt.Fprintf(rw, string(out))
}
